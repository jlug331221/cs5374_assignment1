def verify_phone_number(client, phone_number, license_key=None):
  return client.service.CheckPhoneNumber(phone_number, license_key)

def verify_phone_numbers(client, phone_numbers, license_key=None):
  return client.service.CheckPhoneNumbers(phone_numbers, license_key)
