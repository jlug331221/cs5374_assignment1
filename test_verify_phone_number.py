from unittest import TestCase
import zeep

wsdl = 'http://ws.cdyne.com/phoneverify/phoneverify.asmx?wsdl'
client = zeep.Client(wsdl)

class TestVerify_phone_number(TestCase):
  #####################################
  # TC1
  #####################################
  def test_verify_valid_phone_number_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '2525031948'
    license_key = '12345'
    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertTrue(result.Valid)

  #####################################
  # TC2
  #####################################
  def test_verify_valid_phone_number_without_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '2525031948'
    result = verify_phone_number(client, number_to_verify, '')
    self.assertTrue(result.Valid)

  #####################################
  # TC3
  #####################################
  def test_verify_invalid_phone_number_less_than_10_digits_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '252503' # phone number that is 6 digits in length
    license_key = '12345'
    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertFalse(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')

  #####################################
  # TC4
  #####################################
  def test_verify_invalid_phone_number_greater_than_10_digits_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '252503194812345' # phone number that is 15 digits in length
    license_key = '12345'
    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertTrue(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '2525031948', '')

  #####################################
  # TC8
  #####################################
  def test_verify_no_specified_phone_number_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = ''
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertFalse(result.Valid)
    self.assertEqual(result.OriginalNumber, None, '')

  #####################################
  # TC10 - BVA
  #####################################
  def test_verify_nine_digit_phone_number_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '252503194'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertFalse(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '252503194', '')

  #####################################
  # TC11 - BVA
  #####################################
  def test_verify_valid_eleven_digit_phone_number_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '25250319481'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertTrue(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '2525031948', '')

  #####################################
  # TC12
  #####################################
  def test_verify_invalid_phone_number_consisting_of_non_numerical_characters_with_license_key(
      self):
    from phone_numbers import verify_phone_number

    number_to_verify = 'qwerty'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertFalse(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')

  #####################################
  # TC13
  #####################################
  def test_verify_valid_phone_number_consisting_of_dashes_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '252-503-1948'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertTrue(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '2525031948', '')

  #####################################
  # TC14 - BVA
  #####################################
  def test_verify_invalid_phone_number_consisting_of_a_non_numerical_number_as_the_last_digit_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '252503194a'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertFalse(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '252503194', '')

  #####################################
  # TC15 - BVA
  #####################################
  def test_verify_valid_phone_number_with_the_last_digit_as_a_non_numerical_character_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = '2525031948a'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertTrue(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '2525031948', '')

  #####################################
  # TC16 - BVA
  #####################################
  def test_verify_valid_phone_number_with_the_first_digit_as_a_non_numerical_character_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = 'a2525031948'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertTrue(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '2525031948', '')

  #####################################
  # TC17 - BVA
  #####################################
  def test_verify_invalid_phone_number_with_the_first_digit_as_a_non_numerical_character_with_license_key(self):
    from phone_numbers import verify_phone_number

    number_to_verify = 'a1234567890'
    license_key = '12345'

    result = verify_phone_number(client, number_to_verify, license_key)
    self.assertFalse(result.Valid)
    self.assertEqual(result.OriginalNumber, number_to_verify, '')
    self.assertEqual(result.CleanNumber, '234567890', '')