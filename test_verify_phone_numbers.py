from unittest import TestCase
import zeep

wsdl = 'http://ws.cdyne.com/phoneverify/phoneverify.asmx?wsdl'
client = zeep.Client(wsdl)

class TestVerify_phone_numbers(TestCase):
  #####################################
  # TC5
  #####################################
  def test_verify_valid_phone_numbers_with_license_key(self):
    from phone_numbers import verify_phone_numbers

    numbers_to_verify = ['2525031948', '2525031948']
    license_key = '12345'

    results = verify_phone_numbers(client, numbers_to_verify, license_key)

    if results:
      for result in results:
        self.assertTrue(result.Valid)

  #####################################
  # TC6
  #####################################
  def test_verify_valid_phone_numbers_without_license_key(self):
    from phone_numbers import verify_phone_numbers

    numbers_to_verify = ['2525031948', '2525031948']

    results = verify_phone_numbers(client, numbers_to_verify, '')

    if results:
      for result in results:
        self.assertTrue(result.Valid)

  #####################################
  # TC7
  #####################################
  def test_verify_invalid_phone_numbers_with_license_key(self):
    from phone_numbers import  verify_phone_numbers

    numbers_to_verify = ['252503', '252503']
    license_key = '12345'

    results = verify_phone_numbers(client, numbers_to_verify, license_key)

    if results:
      for result in results:
        self.assertFalse(result.Valid)

  #####################################
  # TC9
  #####################################
  def test_verify_empty_list_of_phone_numbers_with_license_key(self):
    from phone_numbers import verify_phone_numbers

    numbers_to_verify = []
    license_key = '12345'

    results = verify_phone_numbers(client, numbers_to_verify, license_key)

    if results:
      for result in results:
        self.assertFalse(result.Valid)